import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Hangman {
    public enum MoveStatus {
        ALREADY_GUESSED, SUCCESS, FAIL, INVALID
    };

    private final int NUM_OF_WRONG_GUESSES = 8;
    private String picked_word = null;
    Set<String> guessed_letters = new HashSet<>();
    List<Character> result = new ArrayList<>();
    private String current_letter = "";
    private int num_of_fails = 0;

    public Hangman(String generated_word) {
        picked_word = generated_word.toUpperCase();
        for (int i = 0; i < picked_word.length(); i++) {
            if (picked_word.charAt(i) == ' ') {
                result.add(' ');
            } else if (picked_word.charAt(i) == '\'') {
                result.add('\'');

            } else {
                result.add('_');
            }
        }
    }

    public MoveStatus check_letter(String letter) {
        current_letter = letter;
        MoveStatus response;

        if (not_letter(letter)) {
            response = MoveStatus.INVALID;
        }

        else if (letter_is_guessed(letter)) {
            response = MoveStatus.ALREADY_GUESSED;

        } else {
            guessed_letters.add(letter);
            if (word_contains_letter(letter)) {
                response = MoveStatus.SUCCESS;

            } else {
                response = MoveStatus.FAIL;
                num_of_fails++;
            }
        }

        return response;
    }

    private boolean not_letter(String letter) {
        return !letter.matches("[a-zA-Z]+") || letter.length() > 1;
    }

    private boolean letter_is_guessed(String letter) {
        return guessed_letters.contains(letter);
    }

    private boolean word_contains_letter(String letter) {
        return picked_word.contains(letter);
    }

    public void generate_word_display() {

        for (int i = 0; i < picked_word.length(); i++) {
            if (current_letter.equals(Character.toString(picked_word.charAt(i)))) {
                result.set(i, current_letter.charAt(0));
            }
        }

    }

    public String get_guessed_word_so_far() {
        return result.stream().map(String::valueOf).collect(Collectors.joining(" "));
    }

    public int get_number_of_fails() {
        return num_of_fails;
    }

    public boolean end_game_reached () {
        return word_guessed() || num_of_fails == NUM_OF_WRONG_GUESSES;
    }

    public boolean word_guessed() {
        return !result.contains('_');
    }

    public Set<String> get_guessed_letters_so_far () {
       
        return guessed_letters;
    }
    
}
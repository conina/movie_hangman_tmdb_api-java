import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HangmanMain {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int year = 0;
        boolean not_valid_year = true;
        while (not_valid_year) {
            System.out.print("\nEnter a year between 1888 and 2020: ");

            try {
                year = Integer.parseInt(br.readLine());
                if (year >= 1888 && year <= 2020) {
                    not_valid_year = false;
                }
            } catch (Exception e) {
                System.out.println("Invalid input. Try again.");

            }
        }

        HttpWordsRequest obj = new HttpWordsRequest();
        String movie = obj.getMovie(year);

        Hangman hangman = new Hangman(movie);
        DrawHangman hangman_pic = new DrawHangman();
        String letter = null;
        int num_of_fails = hangman.get_number_of_fails();

        System.out.println(
                "This is the Movie Hangman. It's a game of guessing movie titles in a requested year by guessing one letter at a time.");
        hangman_pic.draw(num_of_fails);

        System.out.println(hangman.get_guessed_word_so_far());

        while (!hangman.end_game_reached()) {

            System.out.print("\n\nEnter a letter: ");

            try {
                letter = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();

            }

            Hangman.MoveStatus move_status = hangman.check_letter(letter.toUpperCase());

            if (!hangman.get_guessed_letters_so_far().isEmpty()) {
                System.out.println("Guessed letters so far: " + hangman.get_guessed_letters_so_far());
            }
            ;

            switch (move_status) {

            case SUCCESS:
                hangman.generate_word_display();
                System.out.println("Success!\n");

                break;
            case FAIL:

                System.out.println("Sorry, try again!\n");
                break;

            case ALREADY_GUESSED:
                System.out.println("You already guessed this letter. Try again!\n");
                break;

            case INVALID:
                System.out.println("Invalid input. Try again!\n");
                break;

            }

            hangman_pic.draw(hangman.get_number_of_fails());
            System.out.println(hangman.get_guessed_word_so_far());

        }

        if (hangman.end_game_reached()) {
            if (hangman.word_guessed()) {
                System.out.println("Congratulations! You WON!!!!");
            } else {
                System.out.println("You LOST. THE END!!!!");
                System.out.println("It was the movie - " + movie);
            }

        }

    }

}
public class DrawHangman {

    public void draw (int num_of_fails) {
        switch (num_of_fails) {
            case 0: draw_initial_state();
            break;
            case 1: draw_head();
            break;
            case 2: draw_neck();
            break;
            case 3: draw_lhand();
            break;
            case 4: draw_rhand();
            break;
            case 5: draw_torso();
            break;
            case 6: draw_lleg();
            break;
            case 7: draw_rleg();
            break;
            case 8: draw_the_end();
        }

    }

    public void draw_initial_state() {
        System.out.println("-----------");
        System.out.println("|         |");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("");

    }

    public void draw_head() {
        System.out.println("-----------");
        System.out.println("|         |");
        System.out.println("|         O");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("");

    }

    public void draw_neck() {
        System.out.println("-----------");
        System.out.println("|         |");
        System.out.println("|         O");
        System.out.println("|         |");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("");

    }

    public void draw_lhand() {
        System.out.println("-----------");
        System.out.println("|         |");
        System.out.println("|         O");
        System.out.println("|         |");
        System.out.println("|       --");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("");

    }

    public void draw_rhand() {
        System.out.println("-----------");
        System.out.println("|         |");
        System.out.println("|         O");
        System.out.println("|         |");
        System.out.println("|       -- --");
        System.out.println("|");
        System.out.println("|");
        System.out.println("|");
        System.out.println("");

    }

    public void draw_torso() {
        System.out.println("-----------");
        System.out.println("|         |");
        System.out.println("|         O");
        System.out.println("|         |");
        System.out.println("|      --- ---");
        System.out.println("|         |");
        System.out.println("|");
        System.out.println("|");
        System.out.println("");

    }

    public void draw_lleg() {
        System.out.println("-----------");
        System.out.println("|         |");
        System.out.println("|         O");
        System.out.println("|         |");
        System.out.println("|      --- ---");
        System.out.println("|         |");
        System.out.println("|        /");
        System.out.println("|       /");
        System.out.println("");

    }

    public void draw_rleg() {
        System.out.println("-----------");
        System.out.println("|         |");
        System.out.println("|         O");
        System.out.println("|         |");
        System.out.println("|      --- ---");
        System.out.println("|         |");
        System.out.println("|        / \\");
        System.out.println("|       /   \\");
        System.out.println("");
    }
        public void draw_the_end() {
            System.out.println("-----------");
            System.out.println("|         |");
            System.out.println("|         O");
            System.out.println("| THE END |");
            System.out.println("|      --- ---");
            System.out.println("|         |");
            System.out.println("|        / \\");
            System.out.println("|       /   \\");
            System.out.println("");
        
    }
}
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class HttpWordsRequest {

    /*
     * This class uses The Movie Database (TMDb) API which returns paged json
     * results. Each call returns one page with twenty results and contains a field
     * with the total number of pages. In order to get a random movie, two API calls
     * need to be made. The first call - to get the total number of pages and the
     * second to get the specified page. As there is no way to inqure english titles
     * only, the check is made to ensure that if the title doesn't contain an english word, the API calls
     * will be made again
     */

    private final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

    private String API_KEY = "96c2986ab80db69dd61e49bdbaa95184";

    public String getMovie(int year) throws Exception {

        boolean not_american_movie = true;
        String random_movie = "";
        while (not_american_movie) {
            int random_page = get_random_page_for_year(year);
            random_movie = get_random_movie_for_page(random_page, year);

            if (random_movie.matches("[A-Za-z\\s]+")) {
                // System.out.println("Chosen movie IN IF: " + random_movie);

                not_american_movie = false;
            }
        }

        // System.out.println("Chosen movie:" + random_movie);

        return random_movie;
    }

    // This function returns a random page
    private int get_random_page_for_year(int year) throws Exception {

        HttpRequest request1 = HttpRequest.newBuilder().GET().uri(URI.create(String.format(
                "https://api.themoviedb.org/3/discover/movie?api_key=%s&language=en-US&with_original_language=en&region=us&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&primary_release_year=%d",
                API_KEY, year))).build();

        var response = httpClient.send(request1, HttpResponse.BodyHandlers.ofString());

        String json_string = response.body();

        JsonElement result_json_element = new JsonParser().parse(json_string);
        JsonObject result_json_obj = result_json_element.getAsJsonObject();

        JsonElement num_pages = result_json_obj.get("total_pages");

        // total number of pages
        int num_pages_int = num_pages.getAsInt();

        // random page
        int chosen_random_page = (int) (Math.random() * num_pages_int + 1);

        return chosen_random_page;
    }

    // This function makes an API call with a specified page and a year and
    // returns a random movie from that page
    private String get_random_movie_for_page(int page, int year) throws Exception {

        String chosen_movie = "";
        HttpRequest request2 = HttpRequest.newBuilder().GET().uri(URI.create(String.format(
                "https://api.themoviedb.org/3/discover/movie?api_key=%s&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=%d&primary_release_year=%d",
                API_KEY, page, year))).build();

        var page_response = httpClient.send(request2, HttpResponse.BodyHandlers.ofString());
        String page_response_string = page_response.body();

        JsonElement page_response_json_element = new JsonParser().parse(page_response_string);
        JsonObject page_response_json_object = page_response_json_element.getAsJsonObject();

        JsonArray results = page_response_json_object.getAsJsonArray("results");

        JsonObject random_movie = (JsonObject) results.get((int) Math.random() * 20);

        JsonElement value = random_movie.get("original_title");

        chosen_movie = value.getAsString();
        // System.out.println("Chosen movie1: " + chosen_movie);

        chosen_movie = chosen_movie.replaceAll("[,.!:;?]", "");
        // System.out.println("Chosen movie2: " + chosen_movie);

        // System.out.println("Chosen movie3: " + chosen_movie);

        return chosen_movie;

    }


    

}